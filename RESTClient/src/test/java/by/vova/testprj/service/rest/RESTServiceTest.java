package by.vova.testprj.service.rest;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import by.vova.testprj.model.Department;
import by.vova.testprj.model.Employee;
import by.vova.testprj.service.rest.RESTService;
import static org.mockito.Mockito.*;

public class RESTServiceTest {
	
	private Employee[] employees = null;
	private Employee employee = null;
	private Department[] departments = null;
	private Department department = null;
	
	/*подгатавливаем атрибуты для конструктора класса*/
	private HttpHeaders headers = null;
	private HttpEntity<String> request = null;
	private HttpEntity<Department> requestEntity = new HttpEntity<Department>(department, headers);
	private HttpEntity<Employee> requestEntityWithEmployee = new HttpEntity<Employee>(employee, headers);
	
	private Integer departmentId = 1;
	private Integer employeeId = 1;
	
	private HttpStatus statusCode200 = HttpStatus.OK;
	private HttpStatus statusCode201 = HttpStatus.CREATED;
	
	private RestTemplate restTemplate = mock(RestTemplate.class);
	
	/*создание экземпляра тестируемого класса, внедряем свои атрибуты*/
	private RESTService restService = new RESTService(restTemplate, headers, request, requestEntity, requestEntityWithEmployee);
	
/*------------------------------------------------------------------------------------------------------------------*/
	
	/*1*/
	@Test
	public void testGetEmployeesInDepartment() {
		
		ResponseEntity<Employee[]> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(employees);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/employees/{id}", HttpMethod.GET, request, Employee[].class, departmentId)).thenReturn(re);
		
		Employee[] result = restService.getEmployeesInDepartment(departmentId);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/department/employees/{id}", HttpMethod.GET, request, Employee[].class, departmentId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(employees, result);
	}

	/*2*/
	@Test
	public void testGetDepartmentsWithAverageSalary() {
		
		ResponseEntity<Department[]> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(departments);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/department", HttpMethod.GET, request, Department[].class)).thenReturn(re);
		
		Department[] result = restService.getDepartmentsWithAverageSalary();
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/department", HttpMethod.GET, request, Department[].class);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(departments, result);
	}
	
	/*3*/
	@Test
	public void testGetDepartment() {
		
		ResponseEntity<Department> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(department);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/{id}", HttpMethod.GET, request, Department.class, departmentId)).thenReturn(re);
		
		Department res = restService.getDepartment(departmentId);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/department/{id}", HttpMethod.GET, request, Department.class, departmentId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(department, res);
	}
	
	/*4*/
	@Test
	public void testAddDepartment() {
		
		ResponseEntity<Department> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode201);
		when(re.getBody()).thenReturn(department);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/add", HttpMethod.POST, requestEntity, Department.class)).thenReturn(re);
		
		Department res = restService.addDepartment(department);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/department/add", HttpMethod.POST, requestEntity, Department.class);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(department, res);
	}
	
	/*5*/
	@Test
	public void testDeleteDepartment() {
		
		ResponseEntity<Department> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(department);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/delete/{id}", HttpMethod.GET, request, Department.class, departmentId)).thenReturn(re);
		
		Department res = restService.deleteDepartment(departmentId);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/department/delete/{id}", HttpMethod.GET, request, Department.class, departmentId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(department, res);
	}
	
	/*6*/
	@Test
	public void testAddEmployee() {
		
		ResponseEntity<Employee> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode201);
		when(re.getBody()).thenReturn(employee);
		
		when(restTemplate.exchange(
				"http://localhost:8080/RESTServer/rs/employee/add/{id}",
				HttpMethod.POST, requestEntityWithEmployee, Employee.class, departmentId)).thenReturn(re);
		
		Employee res = restService.addEmployee(employee, departmentId);
		
		verify(restTemplate).exchange(
				"http://localhost:8080/RESTServer/rs/employee/add/{id}",
				HttpMethod.POST, requestEntityWithEmployee, Employee.class, departmentId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(employee, res);
		
	}
	
	/*7*/
	@Test
	public void testDeleteEmployee() {
		
		ResponseEntity<Employee> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(employee);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/employee/delete/{id}", HttpMethod.GET, request, Employee.class, employeeId)).thenReturn(re);
		
		Employee res = restService.deleteEmployee(employeeId);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/employee/delete/{id}", HttpMethod.GET, request, Employee.class, employeeId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(department, res);
	}
	
	/*8*/
	@Test
	public void testGetEmployee() {
		
		ResponseEntity<Employee> re = mock(ResponseEntity.class);
		when(re.hasBody()).thenReturn(true);
		when(re.getStatusCode()).thenReturn(statusCode200);
		when(re.getBody()).thenReturn(employee);
		
		when(restTemplate.exchange("http://localhost:8080/RESTServer/rs/employee/{id}", HttpMethod.GET, request, Employee.class, employeeId)).thenReturn(re);
		
		Employee res = restService.getEmployee(employeeId);
		
		verify(restTemplate).exchange("http://localhost:8080/RESTServer/rs/employee/{id}", HttpMethod.GET, request, Employee.class, employeeId);
		verify(re).hasBody();
		verify(re).getStatusCode();
		verify(re).getBody();
		
		assertSame(department, res);
	}
	
	
}
