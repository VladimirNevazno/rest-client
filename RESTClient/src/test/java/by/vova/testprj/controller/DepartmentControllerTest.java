package by.vova.testprj.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import by.vova.testprj.model.Department;
import by.vova.testprj.model.Employee;
import by.vova.testprj.service.rest.Service;


public class DepartmentControllerTest {
	
	private static Department[] expectedDepartments;
	private static Employee[] expectedEmployees;
	private static Employee expectedEmployee;
	private static HashMap<String, Object> model = new HashMap<String, Object>();
	
	private final static Department department = new Department("test");
	
	private final Integer departmentId = 1;
	private final Integer employeeId = 1;
	
	/*создаём фиктивный объект используя интерфейс*/
	private Service service = mock(Service.class);
	
	/*создаём контроллер, подставляя фиктивный сервис*/
	private DepartmentController controller = new DepartmentController(service);
	
	@BeforeClass
	public static void myBeforeClass() {
		
	expectedEmployee = new Employee(111, "firstname", "lastname", "patronymic", 345, new Department(222, "name", 555, null));	
		
	expectedDepartments = new Department[3];
	expectedDepartments[0] = new Department();
	expectedDepartments[1] = new Department();
	expectedDepartments[2] = new Department();
	
	expectedEmployees = new Employee[3];
	expectedEmployees[0] = new Employee();
	expectedEmployees[1] = new Employee();
	expectedEmployees[2] = new Employee();
	
	}
	
	/*1*/
	@Test
	public void testViewDepartments() {
		
		/*на запрос метода возвращаем подставной объект*/
		when(service.getDepartmentsWithAverageSalary()).thenReturn(expectedDepartments);
		
		/*вызываем обработчик с подставной моделью(MAP)*/
		String viewName = controller.viewDepartments(model);
		
		/*проверяем, возвращается ли нужное название страницы отображения*/
		assertEquals("view_departments", viewName);
		
		/*проверяем, вернулся ли объект идентичный отправленному в сервлет*/
		assertSame(expectedDepartments, model.get("departments"));
		verify(service).getDepartmentsWithAverageSalary();
		
	}

	/*2*/
	/*этот контроллер имеет параметры в пути, просто приму параметр как еденицу*/
	@Test
	public void testViewEmployeesInDepartment() {
		
		/*на запрос метода возвращаем подставной объект*/
		when(service.getEmployeesInDepartment(departmentId)).thenReturn(expectedEmployees);
		
		/*вызываем обработчик с подставной моделью(MAP)*/
		String viewName = controller.viewEmployeesInDepartment(model, departmentId);
		
		/*проверяем, возвращается ли нужное название страницы отображения*/
		assertEquals("view_employees", viewName);
		
		/*проверяем, вернулся ли объект идентичный отправленному в сервлет*/
		assertSame(expectedEmployees, model.get("employees"));
		verify(service).getEmployeesInDepartment(departmentId);
	}
	
	/*3*/
	@Test
	public void testUpdateEmployeeGet() {
		
		/*на запрос метода возвращаем подставной объект*/
		when(service.getEmployee(employeeId)).thenReturn(expectedEmployee);
		
		/*вызываем обработчик с подставной моделью(MAP)*/
		String viewName = controller.updateEmployeeGet(model, employeeId);
		
		/*проверяем, возвращается ли нужное название страницы отображения*/
		assertEquals("update_employee", viewName);
		
		/*проверяем, вернулся ли объект идентичный отправленному в сервлет*/
		assertSame(expectedEmployee, model.get("employee"));
		verify(service).getEmployee(employeeId);
	}
	
	/*4*/
	@Test
	public void testUpdateEmployeePost() {
		
		Employee employee = expectedEmployee;
		
		/*на запрос метода возвращаем подставной объект*/
		when(service.addEmployee(employee, employee.getDepartment().getId())).thenReturn(expectedEmployee);
		/*вызываем обработчик с подставной моделью(MAP)*/
		String viewName = controller.updateEmployeePost(expectedEmployee, employeeId);
		
		/*проверяем, возвращается ли нужное название страницы отображения*/
		assertEquals("redirect:/view_employees/"+expectedEmployee.getDepartment().getId(), viewName);
		
		verify(service).addEmployee(employee, employee.getDepartment().getId());
	}
	
	/*5*/
	@Test
	public void testUpdateDepartmentGet() {
		
		when(service.getDepartment(departmentId)).thenReturn(department);
		
		String viewName = controller.updateDepartment(model, departmentId);
		
		/*проверяем на возврат правильного названия отображения и на возвращаемый объект в модели*/
		assertEquals("update_department", viewName);
		assertSame(department, model.get("department"));
		
		verify(service).getDepartment(departmentId);
	}
	
	/*6*/
	@Test
	public void testUpdateDepartmentPost() {
		
		when(service.addDepartment(department)).thenReturn(department);
		
		String viewName = controller.updateDepartment(department);
		
		/*проверяем на возврат правильного названия отображения и на возвращаемый объект в модели*/
		assertEquals("redirect:/view_departments", viewName);
		
		verify(service).addDepartment(department);
	}
	
	/*7*/
	@Test
	public void testAddDepartmentGet() {
		
		String viewName = controller.addDepartment(model);
		
		assertEquals("add_department", viewName);
		assertTrue(model.get("department") != null);
	}
	
	/*8*/
	@Test
	public void testAddDepartmentPost() {
		
		when(service.addDepartment(department)).thenReturn(department);
		
		String viewName = controller.addDepartment(department, model);
		
		assertEquals("redirect:/view_departments", viewName);
				
		verify(service).addDepartment(department);
	}
	
	/*9*/
	@Test
	public void testAddEmpInDepGet() {
		
		String viewName = controller.addEmpInDepGet(model, departmentId);
		
		assertEquals("create_employee", viewName);
		assertSame(departmentId, model.get("departmentId"));
		assertTrue(model.get("employee") != null);
	}
	
	/*10*/
	@Test
	public void testAddEmpInDepPost() {
		
		when(service.addEmployee(expectedEmployee, departmentId)).thenReturn(expectedEmployee);
		
		String viewName = controller.addEmpInDepPost(model, expectedEmployee, departmentId);
		
		assertEquals("redirect:/view_employees/"+departmentId, viewName);
				
		verify(service).addEmployee(expectedEmployee, departmentId);
	}
	
	/*11*/
	@Test
	public void testDeleteDepartment() {
		
		when(service.deleteDepartment(departmentId)).thenReturn(department);
		
		String viewName = controller.deleteDepartment(model, departmentId);
		
		assertEquals("redirect:/view_departments", viewName);
				
		verify(service).deleteDepartment(departmentId);
	}
	
	/*12*/
	@Test
	public void testDeleteEmployee() {
		
		when(service.deleteEmployee(employeeId)).thenReturn(expectedEmployee);
		
		String viewName = controller.deleteEmployee(model, departmentId, employeeId);
		
		assertEquals("redirect:/view_employees/"+departmentId, viewName);
				
		verify(service).deleteEmployee(employeeId);
	}

}
