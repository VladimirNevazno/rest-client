<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>добавление нового сотрудника</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
</head>
<body>
<h2>departmentId=${departmentId}</h2><br/>

 <sf:form method="post" modelAttribute="employee">

		<table align="center" >
			<tr>
				<td><sf:label path="firstname">firstname</sf:label></td>
				<td><sf:input path="firstname" /></td>
			</tr>
			<tr>
				<td><sf:label path="lastname">lastname</sf:label></td>
				<td><sf:input path="lastname" /></td>
			</tr>
			<tr>
				<td><sf:label path="patronymic">patronymic</sf:label></td>
				<td><sf:input path="patronymic" /></td>
			</tr>
			<tr>
				<td><sf:label path="salary">salary</sf:label></td>
				<td><sf:input path="salary" /></td>
			</tr>
			<tr>
				<td><input type="submit"
					value="Добавить" /></td>
			</tr>
		</table>
		
</sf:form> 

</body>
</html>