﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="cr" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
 <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Сотрудники отдела</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
  </head>
  <body>
  
   <table width="100%" height="70" >
    <tr>
    <th><h1><a href="../create_employee/${departmentId}">создать и добавить сотрудника</a></h1></th>  
   </tr>
   </table>
   
   <table border="1" width="100%" height="70" cellpadding="5">
    <tr>
   	 <th>id</th>
     <th>имя<!-- <spring:message code="label.name" /> --></th>
     <th>отчество</th>
     <th>фамилия</th>
     <th>зарплата</th>
   </tr>
    
   
   <cr:forEach items="${employees}" var="employee">
   	<tr>
   	 <th>${employee.id}</th>
   	 <th>${employee.firstname}</th>
   	 <th>${employee.patronymic}</th>
   	 <th>${employee.lastname}</th>
   	 <th>${employee.salary}</th>
   	 <th><a href="../update_employee/${employee.id}">редактировать</a></th>
     <th><a href="../delete_employee/${departmentId}/${employee.id}">удалить</a></th>
    </tr>
   </cr:forEach>
    
   </table>
   
   <table width="100%" height="70" >
    <tr>
    <th><h1><a href="../view_departments">назад</a></h1></th>  
   </tr>
   </table>
   
  </body>
 </html>