<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>редактирование данных отдела с id=${department.id}.</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
</head>
<body>

	<c:if test="${empty department}">
		<h1>department == null<br/>добавить веб-сервис на сервер.</h1>
	</c:if>

	<c:if test="${!empty department}">
	<table width="100%" >

		<tr>
			<th width="100%">
			<sf:form method="POST"
				action="../update_department"
					modelAttribute="department">
					<fieldset>
						<legend>отдел</legend>
						
						<sf:input path="id" type="hidden" /><br />
						изменить название<sf:input path="name" /><br />
						<sf:input path="averageSalary" type="hidden" />
						
						<br />
					</fieldset>
					
					<input type="submit" value="Добавить в БД >>">

				</sf:form></th>
		</tr>
	</table>
	</c:if>
	
	<table width="100%" >
    <tr>
    <th><h1><a href="../view_departments">назад</a></h1></th>  
   </tr>
   </table>
	
</body>
</html>