<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>добавляем сотрудников в отдел</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
</head>
<body>
	добавляем сотрудников в отдел "${department.name}".
 <table border="1" width="100%" height="70" cellpadding="5">
   <tr>
    <th width="100%">
    
    	
<sf:form method="POST" modelAttribute="department" action="add_emps_in_dep" >
	
	<fieldset>
		<legend>добавление нового сотрудника.</legend>
			
			название <sf:input path="name" size="40" />
			<br/>
			средняя з/п <sf:input path="averageSalary" size="40" />
			<br/>
			<a href="view_emps_in_dep">сотрудники отдела</a>
				
			
	</fieldset>
					
	<input type="submit" value="Добавить в БД >>">					

</sf:form>
    	
    </th>
   </tr>
   </table>
	
	<table width="100%" >
		<tr>
			<th><h1>
					<a href="index">к началу</a>
			</h1></th>
		</tr>
	</table>
	
</body>
</html>