﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="cr" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
 <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>список отделов</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
  </head>
  <body>
   
   <table width="100%" >
   <tr>
    <th><h1><a href="add_department">добавить новый отдел</a></h1></th>  
   </tr>
   </table>
   
   <table border="1" width="100%" height="70" cellpadding="5">
    
    <tr>
   	 <th>название отдела</th>
   	 <th>средняя зарплата</th>
     <th>сотрудники отдела</th>
   </tr>
    
  <cr:forEach items="${departments}" var="item">
   <tr>
   	 <th>${item.name}</th>
   	 <th>${item.averageSalary}</th>
     <th><a href="view_employees/${item.id}">посмотреть сотрудников</a></th>
     <th><a href="update_department/${item.id}">редактировать</a></th>
     <th><a href="delete_department/${item.id}">удалить</a></th>
   </tr>
   </cr:forEach>
    
   </table>
   
   <table width="100%" >
   <tr>
    <th><h1><a href="index">к началу</a></h1></th>  
   </tr>
   </table>
   
  </body>
 </html>