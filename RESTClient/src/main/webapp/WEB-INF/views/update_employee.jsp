<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>редактирование сотрудника.</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
</head>
<body>

	<c:if test="${empty employee}">
		<h1>employee == null<br/>сотрудника с таким id не существует или сервер не выдал данные.</h1>
	</c:if>

	<c:if test="${!empty employee}">
	<table width="100%" >

		<tr>
			<th width="100%">
			<sf:form method="POST"
					modelAttribute="employee">
					<fieldset>
						<legend>сотрудник</legend>
						
						<sf:input path="id" type="hidden"/><br />
						firstname <sf:input path="firstname" /><br />
						lastname  <sf:input path="lastname" /><br />
						patronymic<sf:input path="patronymic" /><br />
						salary    <sf:input path="salary" /><br />
						<sf:input path="department.id" type="hidden"/>
						
					</fieldset>
					
					<input type="submit" value="Добавить в БД >>">

				</sf:form></th>
		</tr>
	</table>
	</c:if>
	
	<table width="100%" >
    <tr>
    <th><h1><a href="../view_departments">назад</a></h1></th>  
   </tr>
   </table>
	
</body>
</html>