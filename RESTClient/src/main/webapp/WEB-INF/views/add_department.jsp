<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>добавление нового отдела</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/mysite.css" />" >
</head>
<body>

	<table width="100%" height="470" >

		<tr>
			<th width="100%">
			<sf:form method="POST"
					modelAttribute="department">
					<fieldset>
						<legend>добавление нового отдела</legend>
						<p>
						название <sf:input path="name" size="40"></sf:input>
						<br />
					</fieldset>
					
					<input type="submit" value="Добавить в БД >>">

				</sf:form></th>
		</tr>
	</table>
	
	<table width="100%" >
	    <tr>
	    	<th><h1><a href="./view_departments">назад</a></h1></th>  
	    </tr>
    </table>

</body>
</html>