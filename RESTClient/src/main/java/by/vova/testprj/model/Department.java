package by.vova.testprj.model;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Department {
	
	private static final Logger logger = LoggerFactory.getLogger(Department.class);
	
	private Integer id;
	private String name;
	private Integer averageSalary;
	
	private List<Employee> employees = null;
	

	public Department() {
		
	}
	
	public Department(String name) {
		this.name = name;
	}
	
	public Department(Integer id, String name, Integer averageSalary,
			List<Employee> employees) {
		
		this.id = id;
		this.name = name;
		this.averageSalary = averageSalary;
		this.employees = employees;
	}

	// Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAverageSalary() {
		return averageSalary;
	}

	public void setAverageSalary(Integer averageSalary) {
		this.averageSalary = averageSalary;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", averageSalary="+averageSalary+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {logger.info("this == obj"); return true;}
        if (obj == null) {logger.info("obj == null"); return false;}
        if (this.getClass() != obj.getClass()) {
        	logger.info("getClass() != obj.getClass()\nthis.getClass(): "+this.getClass()+"\n != \nobj.getClass(): "+obj.getClass());
        	return false;
        	}
        
        Department other = (Department) obj;
        if (!this.id.equals(other.getId())) {logger.info("id = false"); return false;}
        if (!this.name.equals(other.getName())) {logger.info("name = false"); return false;}
        if (this.averageSalary != null && other.getAverageSalary() != null) {
        	logger.info("this as = "+this.averageSalary+"expect as = "+other.getAverageSalary());
        	if (!this.averageSalary.equals(other.getAverageSalary())) {logger.info("as = false"); return false;}
        }
        return true;
	}
	
}
