package by.vova.testprj.model;




public class Employee {

	private Integer id;

	private String firstname;

	private String lastname;
	
	private String patronymic;

	private Integer salary;
	
	private Department department;

	
	public Employee() {
		
	}
	
	/*конструктор специально для теста. id назначается Hibernate*/
	public Employee(Integer id, String firstname, String lastname, String patronymic,
			Integer salary, Department department) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.patronymic = patronymic;
		this.salary = salary;
		this.department = department;
	}

	public Employee(String firstname, String lastname, String patronymic,
			Integer salary, Department department) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.patronymic = patronymic;
		this.salary = salary;
		this.department = department;
	}

	// Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", patronymic=" + patronymic
				+ ", salary=" + salary + ", department=" + department + "]";
	}

	
}
