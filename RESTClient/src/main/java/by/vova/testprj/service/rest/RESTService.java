package by.vova.testprj.service.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import by.vova.testprj.model.Department;
import by.vova.testprj.model.Employee;

/*добавил авторизацию*/
/*подключаем бин restTemplate,создаём объект заголовков, добавляем в него заголовок, 
 * создаём объект в который помещаем заголовки
 * */
public class RESTService implements Service {

	private static final Logger logger = LoggerFactory.getLogger(RESTService.class);
	
	private RestTemplate restTemplate;
	private HttpHeaders headers;
	private HttpEntity<String> request;
	private HttpEntity<Department> requestEntity;
	private HttpEntity<Employee> requestEntityWithEmployee;

	
	/*конструктор для тестирования*/
	public RESTService(RestTemplate restTemplate, HttpHeaders headers,
			HttpEntity<String> request, HttpEntity<Department> requestEntity, HttpEntity<Employee> requestEntityWithEmployee) {
		this.restTemplate = restTemplate;
		this.headers = headers;
		this.request = request;
		this.requestEntity = requestEntity;
		this.requestEntityWithEmployee = requestEntityWithEmployee;
	}
	
	/*в стандартном конструкторе добавляются данные авторизации на сервере*/
	public RESTService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
		headers = new HttpHeaders();
		/* login:pass - http://base64.ru/ - обработанныйлогин:пароль*/	
		headers.add("Authorization", "Basic dXNlcjE6MTExMQ=="); 	
		request = new HttpEntity<String>(headers);
	}
	
/*-----------------------------------------------------------------------------------------------*/
	
	/*1. получаем список сотрудников в отделе*/
	public Employee[] getEmployeesInDepartment(Integer departmentId) {
		
		Employee[] employees = null;
		ResponseEntity<Employee[]> re = null;
		
		try {
			re = restTemplate.exchange(
					"http://localhost:8080/RESTServer/rs/department/employees/{id}",
					HttpMethod.GET, request, Employee[].class, departmentId);
			
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				employees = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return employees;
	}

	/*2. получаем список отделов с высчитанной средней зарплатой*/
	public Department[] getDepartmentsWithAverageSalary() {
		
		Department[] departments = null;
		ResponseEntity<Department[]> re = null;
		
		try {
			re = restTemplate.exchange(
					"http://localhost:8080/RESTServer/rs/department",
					HttpMethod.GET, request, Department[].class);
			
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				departments = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return departments;
	}

	/*3. получаем отдел с высчитанной средней зарплатой*/
	public Department getDepartment(Integer departmentId) {
		
		Department responseDepartment = null;
		ResponseEntity<Department> re = null;
		
		try {
			re = restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/{id}", HttpMethod.GET, request, Department.class, departmentId);
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				responseDepartment = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return responseDepartment;
	}
	
	/*4. создаём отдел*/
	public Department addDepartment(Department department) {
		ResponseEntity<Department> re = null;
		
		/*если идёт JUnit-тестирование, то requestEntity передаётся через конструктор.*/
			
			if (requestEntity == null) {requestEntity = new HttpEntity<Department>(department, headers);}
		
		try {
			re = restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/add", HttpMethod.POST, requestEntity, Department.class);
			
			requestEntity = null;
			
			if (re.hasBody() && re.getStatusCode().toString().equals("201")) {
				return re.getBody();
			}

		} catch (RestClientException e) {
			requestEntity = null;
		}

		return null;

	}
	
	/*написать тест*/
	/*5. удаление отдела по id*/
	public Department deleteDepartment(Integer departmentId) {
		
		Department responseDepartment = null;
		ResponseEntity<Department> re = null;
		
		try {
			re = restTemplate.exchange("http://localhost:8080/RESTServer/rs/department/delete/{id}", HttpMethod.GET, request, Department.class, departmentId);
		
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				responseDepartment = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return responseDepartment;
	}
	
	/*написать тест*/
	/*6. отправляем нового сотрудника на сервер или изменяем имеющегося.*/
	public Employee addEmployee(Employee employee, Integer departmentId) {
		
		ResponseEntity<Employee> re = null;
		Employee responseEmployee = null;
		
		if (requestEntityWithEmployee == null) {requestEntityWithEmployee = new HttpEntity<Employee>(employee, headers);}
		
		try {
			re = restTemplate.exchange(
					"http://localhost:8080/RESTServer/rs/employee/add/{id}",
					HttpMethod.POST, requestEntityWithEmployee, Employee.class, departmentId);
			
			requestEntityWithEmployee = null;
			
			if (re.hasBody() && re.getStatusCode().toString().equals("201")) {
				responseEmployee = re.getBody();// принимаем объект от сервера
				logger.info("ответ 201 (ресурс создан.)");
				return responseEmployee;
			}

		} catch (RestClientException e) {
			requestEntityWithEmployee = null;
		}

		return null;

	}
	
	/*написать тест*/
	/*7. удаление сотрудника*/
	public Employee deleteEmployee(Integer employeeId) {
		
		Employee responseEmployee = null;
		ResponseEntity<Employee> re = null;
		
		try {
			re = restTemplate.exchange("http://localhost:8080/RESTServer/rs/employee/delete/{id}", HttpMethod.GET, request, Employee.class, employeeId);
		
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				responseEmployee = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return responseEmployee;
	}

	/*написать тест*/
	/*8. получить сотрудника по id*/
	public Employee getEmployee(Integer employeeId) {
		
		Employee responseEmployee = null;
		ResponseEntity<Employee> re = null;
		
		try {
			re = restTemplate.exchange("http://localhost:8080/RESTServer/rs/employee/{id}", HttpMethod.GET, request, Employee.class, employeeId);
			
			if (re.hasBody() && re.getStatusCode().toString().equals("200")) {
				responseEmployee = re.getBody();
			}
			
		} catch (RestClientException e) {}
		return responseEmployee;
	}


}
