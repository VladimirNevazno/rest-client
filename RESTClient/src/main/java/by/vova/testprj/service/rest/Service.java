package by.vova.testprj.service.rest;

import by.vova.testprj.model.Department;
import by.vova.testprj.model.Employee;

public interface Service {
	
	/*1. получаем список сотрудников в отделе*/
	public Employee[] getEmployeesInDepartment(Integer departmentId);
	
	/*2. получаем список отделов с высчитанной средней зарплатой*/
	public Department[] getDepartmentsWithAverageSalary();
	
	/*3. получаем отдел с высчитанной средней зарплатой*/
	public Department getDepartment(Integer departmentId);
	
	/*4. создаём отдел*/
	public Department addDepartment(Department department);

	/*5. удаление отдела по id*/
	public Department deleteDepartment(Integer departmentId);
	
	/*6. отправляем нового сотрудника на сервер*/
	public Employee addEmployee(Employee employee, Integer departmentId);
	
	/*7. удаление сотрудника*/
	public Employee deleteEmployee(Integer employeeId);
	
	/*8. получить сотрудника по id*/
	public Employee getEmployee(Integer employeeId);
	
}
