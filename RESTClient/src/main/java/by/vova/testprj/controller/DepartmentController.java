package by.vova.testprj.controller;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import by.vova.testprj.model.Department;
import by.vova.testprj.model.Employee;
import by.vova.testprj.service.rest.Service;


@Controller
public class DepartmentController {
	
	private static final Logger logger = LoggerFactory.getLogger(DepartmentController.class);
	
	private Service service;
	
	@Autowired
	public DepartmentController(Service service) {
		this.service = service;
	}
	
	/*1*/
	/*получаем список отделов со средней зарплатой сотрудников*/
	@RequestMapping(value="/view_departments", method = RequestMethod.GET)
	public String viewDepartments(Map<String, Object> model) {
		logger.info("получить отделы.");
		model.put("departments", service.getDepartmentsWithAverageSalary());
		return "view_departments";
	}
	
	/*2*/
	/*чтение списка сотрудников в определённом отделе*/
	@RequestMapping(value="/view_employees/{id}", method = RequestMethod.GET)
	public String viewEmployeesInDepartment(Map<String, Object> model,@PathVariable Integer id) {
		logger.info("получить сотрудников из отдела с id="+id);
		model.put("departmentId", id);
		model.put("employees", service.getEmployeesInDepartment(id));
		
		return "view_employees";
	}

/*---изменение сотрудника------------------------------------------------------------------------------------------------*/	
	
	/*3*/
	/*редактирование сотрудника*/ ///{departmentId}
	@RequestMapping(value="/update_employee/{employeeId}", method = RequestMethod.GET)
	public String updateEmployeeGet(Map<String, Object> model, @PathVariable(value="employeeId") Integer employeeId) {
		logger.info("редактировать сотрудника с id="+employeeId);
		
		Employee employee = service.getEmployee(employeeId);
		model.put("employee", employee);
		
		return "update_employee";
	}
	
	/*4*/
	/*редактирование сотрудника*/
	@RequestMapping(value="/update_employee/{id}", method = RequestMethod.POST)
	public String updateEmployeePost(@ModelAttribute("employee") Employee employee, @PathVariable Integer id) {
		
		Employee result = service.addEmployee(employee, employee.getDepartment().getId());
		
		logger.info("добавили сотрудника на сервер: "+result);
		
		return "redirect:/view_employees/"+employee.getDepartment().getId();
	}
	
/*---изменение отдела----------------------------------------------------------------------------------------------------*/	
	
	/*5*/
	/*редактирование отдела*/
	@RequestMapping(value="/update_department/{id}", method = RequestMethod.GET)
	public String updateDepartment(Map<String, Object> model, @PathVariable Integer id) {
		Department department = service.getDepartment(id);
		model.put("department", department);
		logger.info("отправили редактировать отдел: "+department+", в update_department.jsp");
		return "update_department";
	}
	
	/*6*/
	/*в этом методе id отдела передаётся вместе с объектом из скрытого поля формы*/
	/*редактирование отдела*/
	@RequestMapping(value="/update_department", method = RequestMethod.POST)
	public String updateDepartment(@ModelAttribute("department") Department department) {
		
		Department result = service.addDepartment(department);
		
		logger.info("добавили отдел на сервер: "+result);
		
		return "redirect:/view_departments";
	}
	
/*---добавление отдела---------------------------------------------------------------------------------------------------*/	

	/*7*/
	/*создание отдела*/
	@RequestMapping(value="/add_department", method = RequestMethod.GET)
	public String addDepartment(Map<String, Object> model) {
		
		model.put("department", new Department());
		
		logger.info("отправлили новый отдел в форму для добавления инфы в add_department.jsp");
		return "add_department";
	}
	
	/*8*/
	/*принимаем форму с данными нового отдела*/
	@RequestMapping(value="/add_department", method = RequestMethod.POST)
	public String addDepartment(@ModelAttribute("department") Department department, Map<String, Object> model) {
		
		logger.info("Добавили новый отдел.");
		service.addDepartment(department);
		
		return "redirect:/view_departments";
	}
	
/*---добавление сотрудника в отдел---------------------------------------------------------------------------------------*/	

	/*9*/
	/*добавляем сотрудника в отдел.*/
	@RequestMapping(value="/create_employee/{id}", method = RequestMethod.GET)
	public String addEmpInDepGet(Map<String, Object> model, @PathVariable Integer id) {
		
		model.put("departmentId", id);
		Employee employee = new Employee();
		model.put("employee", employee);
		logger.info("добавляем сотрудника в отдел.");
		return "create_employee";
	}
	
	/*10*/
	/*добавляем сотрудника в отдел.*/
	@RequestMapping(value="/create_employee/{id}", method = RequestMethod.POST)
	public String addEmpInDepPost(Map<String, Object> model, @ModelAttribute("employee") Employee employee, @PathVariable Integer id) {
		
		service.addEmployee(employee, id);
		logger.info("создан новый сотрудник в отделе с id="+id);
		return "redirect:/view_employees/"+id;
	}
	
/*-----------------------------------------------------------------------------------------------------------------------*/

	/*11*/
	/*удаление отдела*/
	@RequestMapping(value="/delete_department/{id}", method = RequestMethod.GET)
	public String deleteDepartment(Map<String, Object> model, @PathVariable Integer id) {
		logger.info("удаляем отдел с id="+id);
		
		service.deleteDepartment(id);
		
		return "redirect:/view_departments";
	}
	
	/*12*/
	/*удаление сотрудника*/
	@RequestMapping(value="/delete_employee/{departmentId}/{employeeId}", method = RequestMethod.GET)
	public String deleteEmployee(Map<String, Object> model, @PathVariable(value="departmentId") Integer departmentId
			, @PathVariable(value="employeeId") Integer employeeId) {
		logger.info("удаляем сотрудника с employeeId="+employeeId+", из отдела с departmentId="+departmentId);
		
		service.deleteEmployee(employeeId);
		
		return "redirect:/view_employees/"+departmentId;
	}
	
}